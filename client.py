import socket

def start_client():
    host = 'localhost'
    port = 12345

    client_socket = socket.socket()
    client_socket.connect((host, port))

    num = int(client_socket.recv(1024).decode())
    print(f"Received from server: {num}")

    while num <= 99:
        num += 1
        print(f"Sending to server: {num}")
        client_socket.send(str(num).encode())

        if num < 100:
            data = client_socket.recv(1024).decode()
            print(f"Received from server: {data}")
            num = int(data)
        else:
            break

    client_socket.close()

if __name__ == "__main__":
    start_client()
