import socket

def start_server():
    host = 'localhost'
    port = 12345

    server_socket = socket.socket()
    server_socket.bind((host, port))

    server_socket.listen(1)
    print("Waiting for connection from the client...")

    conn, addr = server_socket.accept()
    print(f"Connected to {addr}")

    num = 1
    print(f"Sending to client: {num}")
    conn.send(str(num).encode())

    while num <= 100:
        data = conn.recv(1024).decode()
        print(f"Received from client: {data}")

        num = int(data) + 1

        if num <= 100:
            print(f"Sending to client: {num}")
            conn.send(str(num).encode())

    conn.close()

if __name__ == "__main__":
    start_server()
